# hotelapp

## Project setup
```
npm install
```

### Starts node web server and REST API
```
npm run dev
```

### Compiles and minifies for production
```
npm run build
```

### Checks the app inside a browser
```
http://localhost:3000
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
