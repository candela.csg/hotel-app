import Vue from 'vue';
import Router from 'vue-router';
import Main from './views/ChoosePlan.vue';

Vue.use(Router);

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'choosePlan',
      component: Main,
    },
    {
      path: '/extras',
      name: 'chooseExtras',
      component: () => import('./views/ChooseExtras.vue'),
    },
  ],
});
