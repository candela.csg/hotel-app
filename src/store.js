import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    hotelID: 1,
    hotelData: {},
    // In a real app, user data would be saved in session/local storage by a login component
    userID: 7,
  },
  getters: {
    essentialData: state => (({ id, name, rating }) => ({ id, name, rating }))(state.hotelData),
    plans: state => state.hotelData.plans,
    stayImprovements: state => state.hotelData.stay_improvements,
    roomServices: state => state.hotelData.room_services,
  },
  mutations: {
    setHotelData(state, hotelData) {
      state.hotelData = hotelData;
    },
  },
  // All of the console.log statements below would be substituted for user warnings, redirections...
  actions: {
    async loadHotelData({ state, commit }) {
      try {
        const response = await fetch(`http://localhost:3000/api/hotels/${state.hotelID}`);
        if (response.ok) {
          const hotelData = await response.json();
          commit('setHotelData', hotelData);
        } else {
          throw new Error(`Server response is NOT OK: ${response.status} ${response.statusText}.`);
        }
      } catch (error) {
        console.log(`Network or server error -> ${error}`);
      }
    },
    async checkoutPlan(context, planID) {
      try {
        const options = {
          method: 'POST',
          headers: { 'Content-Type': 'application/json' },
          body: JSON.stringify({
            user_id: context.state.userID,
            plan_id: planID,
          }),
        };
        const response = await fetch('http://localhost:3000/api/checkouts', options);
        if (response.ok) {
          const succesfulRes = await response.json();
          console.log(`Checkout succesful: ${succesfulRes.message}`);
        } else {
          throw new Error(`Server response is NOT OK: ${response.status} ${response.statusText}.`);
        }
      } catch (error) {
        console.log(`Network or server error -> ${error}`);
      }
    },
  },
});
