const { Router } = require('express');
const router = Router();
const fs = require('fs');
const path = require('path');

const checkoutsJsonPath = path.join(__dirname, '../data/checkouts.json');
const checkouts = require('./../data/checkouts.json');

// This serves as the simplest example of a POST action
// With each succesful POST request, one new registry is added to /server/data/checkouts.json
router.post('/', (req, res) => {
  const newCheckout = {
    id: checkouts.length + 1,
    date: new Date().toString(),
    ...req.body,
  };
  checkouts.push(newCheckout);
  fs.writeFileSync(checkoutsJsonPath, JSON.stringify(checkouts), 'utf8');
  res.status(201).json({
    message: `The checkout #${newCheckout.id} has been created`,
    content: newCheckout,
  });
});

module.exports = router;
