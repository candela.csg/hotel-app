const { Router } = require('express');
const router = Router();

const hotels = require('./../data/hotels.json');

// Get all (Example, not used)
router.get('/', (req, res) => res.json(hotels));

// Get one
router.get('/:id', (req, res) => res.json(hotels.find(hotel => hotel.id === req.params.id)));

// The rest of the common HTTP methods: POS, PUT, PATH and DELETE, would go here

// router.post('/', (req, res) => res.send('Received a POST HTTP method'));
// router.put('/', (req, res) => res.send('Received a PUT HTTP method'));
// router.path('/', (req, res) => res.send('Received a Path HTTP method'));
// router.delete('/', (req, res) => res.send('Received a DELETE HTTP method'));

module.exports = router;
