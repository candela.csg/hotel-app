require('dotenv').config();
const path = require('path');
const express = require('express');
// Needed to use history mode in Vue router
const history = require('connect-history-api-fallback');

const app = express();

// WEB SERVER
app.use('/', express.static(path.join('__dirname', '../dist')));

// REST API
app.use((req, res, next) => {
  // Everyone and everything wellcome! (Would change in production)
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', '*');
  next();
});

app.use(express.json());

// REST API Routes
const hotelRouter = require('./routes/hotels');
app.use('/api/hotels', hotelRouter);

const checkoutRouter = require('./routes/checkouts');
app.use('/api/checkouts', checkoutRouter);


// SERVER INIT
app.use(history());
app.listen(process.env.PORT, () => {
  console.log(`Hotel app web server is listening on port ${process.env.PORT}`);
});
